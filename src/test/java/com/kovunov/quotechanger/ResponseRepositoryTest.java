package com.kovunov.quotechanger;

import com.kovunov.quotechanger.entity.Quote;
import com.kovunov.quotechanger.entity.Response;
import com.kovunov.quotechanger.repository.ResponseRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ResponseRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ResponseRepository repository;

    @Test
    public void whenSaveResponseForTheQuoteItIsSaved() {
        Quote quote = new Quote("Hello my friend");
        entityManager.persist(quote);
        entityManager.flush();

        Response response = new Response("nice quote bro", quote);
        entityManager.persist(response);
        entityManager.flush();
        List<Response> savedResponses = repository.findResponsesByQuote(quote.getId());
        assertTrue(savedResponses.contains(response));
    }
}
