package com.kovunov.quotechanger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kovunov.quotechanger.dto.TextDto;
import com.kovunov.quotechanger.service.QuoteService;
import com.kovunov.quotechanger.service.ResponseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class QuoteControllerTest {

    private static final String API_URL = "/api/quote/";
    private static final TextDto HELLO_REQUEST = new TextDto("hello");
    private static final String JSON_TEXT_EXP = "$.text";
    private static final String HELLO_RESPONSE = "hello";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuoteService quoteService;

    @MockBean
    private ResponseService responseService;

    @Test
    public void postResponseText() throws Exception
    {
        mockMvc.perform(post(API_URL + "/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(HELLO_REQUEST)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(JSON_TEXT_EXP, is(HELLO_RESPONSE)));
    }

    @Test
    public void postQuoteText() throws Exception
    {
        mockMvc.perform(post(API_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(HELLO_REQUEST)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text", is(HELLO_RESPONSE)));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
