package com.kovunov.quotechanger;

import com.kovunov.quotechanger.entity.Quote;
import com.kovunov.quotechanger.repository.QuoteRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class QuoteRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private QuoteRepository repository;

    @Test
    public void whenSaveQuoteThenCanRetrieveIt() {
        Quote quote = new Quote("Hello my friend");
        entityManager.persist(quote);
        entityManager.flush();

        List<Quote> quotes = new ArrayList<>();
        repository.findAll().forEach(quotes::add);
        assertEquals(quote, quotes.get(0));
    }
}
