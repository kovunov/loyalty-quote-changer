import * as reducers from './reducers'
import { CHANGE_USER`NAME, CITY_TEMP_REQUEST_SUCCESS, CITY_TEMP_REQUEST_PENDING } from './constants'

const initialUserNameState = {
  userName: ''
}
describe('change userName reducer tests', () => {
  it('should return the initial state', () => {
    expect(reducers.changeUserName(undefined, {})).toEqual(
      {
        userName: ''
      }
    )
  })

  it('should handle CHANGE_USERNAME', () => {
    expect(
      reducers.changeUserName(initialUserNameState, {
        type: CHANGE_USERNAME,
        payload: 'abc'
      })
    ).toEqual(
      {
        userName: 'abc'
      }
    )
  })
})
