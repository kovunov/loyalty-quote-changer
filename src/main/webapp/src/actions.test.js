import * as actions from './actions'
import { CHANGE_USERNAME, CITY_TEMP_REQUEST_PENDING } from './constants'
import configureMockStore from 'redux-mock-store'
import thunkMiddleware from 'redux-thunk'

export const mockStore = configureMockStore([thunkMiddleware]);

describe('actions', () => {
  it('should create an action to populateUserName', () => {
    const text = 'Antonio'
    const expectedAction = {
      type: CHANGE_USERNAME,
      payload: text
    }
    expect(actions.setUserName(text)).toEqual(expectedAction)
  })
})

describe("Fetch city search Pending", () => {
  it("Should create a Pending action on request to get city from weather API", () => {
    const store = mockStore();
    const mockCityId = "234"
    store.dispatch(actions.getCityTemperature(mockCityId));
    const action = store.getActions();
    expect(action[0]).toEqual({type: CITY_TEMP_REQUEST_PENDING});
  });
});