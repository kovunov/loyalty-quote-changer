import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { changeUserName, searchCityInfo } from './reducers'


const logger = createLogger();

const userFormReducers = combineReducers({changeUserName, searchCityInfo});

const store = createStore(userFormReducers, applyMiddleware(thunkMiddleware, logger));

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);

