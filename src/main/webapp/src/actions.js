import axios from 'axios'
import {
    CHANGE_USERNAME,
    CITY_TEMP_REQUEST_PENDING,
    CITY_TEMP_REQUEST_SUCCESS,
    API_KEY,
    WEATHER_WEBSITE_URL
 } from './constants'

export const setUserName = (text) => ({ type: CHANGE_USERNAME, payload: text })

export const getCityTemperature = (cityId) => async (dispatch) => {
  dispatch({type: CITY_TEMP_REQUEST_PENDING});
  const cityData = await axios.get(`${WEATHER_WEBSITE_URL}id=${cityId}&units=metric&APPID=${API_KEY}`);
  dispatch({ type: CITY_TEMP_REQUEST_SUCCESS, payload: cityData });
}