import React from 'react';
import CityInput from '../CityInput/CityInput'

export default function UserLoginForm({onEnterUserName, onSubmitInfo}) {
    return (
        <div className="col">
            <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon1">User name: </span>
                </div>
                <input type="text" className="form-control" 
                        placeholder="Username" aria-label="Username" 
                        onChange={onEnterUserName} 
                        aria-describedby="basic-addon1"/>
            </div>
             <div className="input-group mb-3">
                 <div className="input-group-prepend">
                    <span className="input-group-text" id="basic-addon1">City: </span>
                </div>
                <CityInput className="form-control" 
                onSubmitInfo={onSubmitInfo} />
            </div>
        </div>
    )
}
