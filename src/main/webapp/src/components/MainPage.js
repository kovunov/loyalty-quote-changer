import React, { Component } from 'react';
import UserLoginForm from '../components/UserLoginForm/UserLoginForm';
import PostForm from '../components/Post/PostForm'

export default class MainPage extends Component {

    render() {
        const { isPending, userName, city } = this.props;
        return (
            <div className="row">
                { isPending ? <UserLoginForm {...this.props}/> : ''}
                <div className="col">
                { !isPending ? <PostForm userName={userName} city={city}/> : ''}
                </div>
            </div>
        )
    }
}
