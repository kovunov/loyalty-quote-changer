import React from 'react'

export default function Response({response}) {
    return (
        <div className="border border-1 bg-light ml-3 mt-2">
          {response.text}
        </div>
    )
}
