import React, { Component } from 'react';
import PostList from './PostList';
import axios from 'axios';

export default class PostForm extends Component {
    constructor(props) {
        super(props);

        const cityInfo = {
            name: props.city.name,
            latitude: props.city.coord.lat,
            longtitude: props.city.coord.lon,
            temperature: props.city.main.temp
        }
    
        this.state = {
          postText: '',
          posts: [],
          city: cityInfo
        };
      }
    async componentDidMount() {
        const responseBody = await axios.get('api/quote');
        this.setState({posts: responseBody.data});
    }
    
    onPostSubmission = async () => {
        const responseBody = await axios.post('api/quote', {text: this.state.postText});
        this.setState({posts: [...this.state.posts, responseBody.data]});
        this.setState({postText: ''});
    }
    render() {
        return (
            <div className="col">
                <textarea className="form-control" value={this.state.postText} 
                    onChange={(event) => this.setState({postText: event.target.value})}/>    
                <button className="btn btn-primary mt-3"  onClick={this.onPostSubmission}>Create Post</button>
                <PostList posts={this.state.posts} city={this.state.city} userName={this.props.userName}/>
            </div>
        )
    }
}
