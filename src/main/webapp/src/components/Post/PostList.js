import React from 'react';
import Post from './Post';

export default function PostList({posts, userName, city}) {
    return (
        <div className="container mt-3">
              <ul className="list-group">
                {posts.map(post => (
                    <li key={post.id} className="list-group-item border-0"><Post post={post} city={city} userName={userName}/></li>
                ))}
              </ul>
        </div>
    )
}

