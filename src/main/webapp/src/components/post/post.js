import React, { Component } from 'react'
import TimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en'
import Response from '../Response/Response'
import PropTypes from 'prop-types'
import axios from 'axios'

export default class Post extends Component {

    static propTypes = {
        responses: PropTypes.instanceOf(Array)
    };

    static defaultProps = {
        responses: []
    };
    constructor(props) {
        super(props);
        this.state = {
            responseText: '',
            responses: [...props.responses]
        };
    }

    addResponse = async () => {
       if (this.state.responseText) {
        const responseBody = await axios.post(`api/quote/${this.props.post.id}`, {text: this.state.responseText});
        this.setState({responses : [...this.state.responses, responseBody.data]});
        this.setState({responseText: ''});
       }
    }
    render() {
        const { userName, city, post } = this.props;
        TimeAgo.addLocale(en)
        const timeAgo = new TimeAgo('en-US')
        return (
            <div className="list-group-item list-group-item-action flex-column align-items-start text-dark bg-warning">
                <div>
                    <div className="d-flex w-100 justify-content-between">
                    <h5 className="mb-1">Post by {userName}</h5>
                    <small>{timeAgo.format(new Date(post.createDateTime))}</small>
                    </div>
                    <p className="mb-1">{post.text}</p>
                    <small>{`City: ${city.name}, Cooridnates: ${city.longtitude}, 
                    ${city.latitude}, Temperature: ${city.temperature} °C`}</small>
                    <div className="d-flex justify-content-between">
                            <textarea className="mt-2 pr-4" value={this.state.responseText} onChange={(event) => this.setState({responseText: event.target.value})}/>
                            <button className="btn btn-primary mt-2 float-right" 
                            onClick={this.addResponse}>Respond</button>
                    </div>
                </div>
                <div>
                        {this.state.responses.map(response => (
                            <Response key={response.id} response={response}/>
                        ))}
                </div>
            </div>
        )
    }
}

