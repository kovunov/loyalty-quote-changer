import React, { Component, Fragment } from "react";
import './CityInput.css';
import cities from '../../cities.json';

class CityInput extends Component {

  constructor(props) {
    super(props);
    this.state = {
      suggestions: cities.map(city => city.name),
      cityId : -1,
      disableSubmit: true,
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: ""
    };
  }  

  onChange = e => {
    const userInput = e.currentTarget.value;

    const filteredSuggestions = this.state.suggestions.filter(
      suggestion =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value
    }, () => {
      this.setSelectedCityId(this.state.userInput);
    });
  };

  onClick = e => {
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText,
    }, () => {
      this.setSelectedCityId(this.state.userInput);
    });
  };

  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion]
      }, () => {
        this.setSelectedCityId(this.state.userInput);
      })
    }

    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  setSelectedCityId = cityName => {
    const selectedCity = cities.find(city => city.name === this.state.userInput);
    if (selectedCity) {
      this.setState({cityId: selectedCity.id, disableSubmit: false});
    }
  }

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput
      }
    } = this;

    let suggestionsListComponent;

    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {filteredSuggestions.map((suggestion, index) => {
              let className;

              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return (
                <li
                  className={className}
                  key={suggestion}
                  onClick={onClick}
                >
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <div className="no-suggestions">
            <em>No suggestions, you're on your own!</em>
          </div>
        );
      }
    }

    return (
      <Fragment>
        <input
          type="text"
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
        />
        {suggestionsListComponent}
        <div className="col text-center">
                <button className="btn btn-primary m-3" disabled={this.state.disableSubmit}
                 onClick={() => this.props.onSubmitInfo(this.state.cityId)}>Submit User</button>
        </div>
      </Fragment>
    );
  }
}

export default CityInput;