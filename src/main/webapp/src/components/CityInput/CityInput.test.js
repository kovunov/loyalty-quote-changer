import React from 'react';
import { shallow } from 'enzyme';
import CityInput from './CityInput';

it('renders without crashing', () => {
  expect(shallow(<CityInput/>)).toMatchSnapshot();
});