import {
    CHANGE_USERNAME,
    CITY_TEMP_REQUEST_PENDING,
    CITY_TEMP_REQUEST_SUCCESS
   } from './constants';
  
  const initialUserNameState = {
    userName: ''
  }
  
  export const changeUserName = (state=initialUserNameState, action={}) => {
    switch (action.type) {
      case CHANGE_USERNAME:
        return Object.assign({}, state, {userName: action.payload})
      default:
        return state
    }
  }
  
  const initialCityState = {
    city: {},
    isPending: true
  }
  
  export const searchCityInfo = (state=initialCityState, action={}) => {
    switch (action.type) {
      case CITY_TEMP_REQUEST_PENDING:
        return Object.assign({}, state, {isPending: true})
      case CITY_TEMP_REQUEST_SUCCESS:
        return Object.assign({}, state, {city: action.payload.data, isPending: false})
      default:
        return state
    }
  }