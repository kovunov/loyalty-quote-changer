import axios from 'axios';

export const postApi = () => axios.create({
  baseURL: `api/quote/`
});