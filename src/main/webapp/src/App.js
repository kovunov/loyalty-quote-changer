import React, { Component } from 'react';
import MainPage from './components/MainPage'
import { connect } from 'react-redux';
import { getCityTemperature, setUserName} from './actions'

const mapStateToProps = (state) => {
  return {
    userName: state.changeUserName.userName,
    city: state.searchCityInfo.city,
    isPending: state.searchCityInfo.isPending
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onEnterUserName: (event) => dispatch(setUserName(event.target.value)),
    onSubmitInfo: (cityId) => dispatch(getCityTemperature(cityId))
  }
}

class App extends Component { 
  render() {
    return (
        <div className="container">
          <MainPage {...this.props}/>
        </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
