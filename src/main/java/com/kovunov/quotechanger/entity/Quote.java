package com.kovunov.quotechanger.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "quote")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Quote {

    @Id
    @GeneratedValue
    private int id;

    @Lob
    private String text;

    @OneToMany(mappedBy = "quote",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Response> responses;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Quote(String text) {
        this.text = text;
    }
}
