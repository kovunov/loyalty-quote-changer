package com.kovunov.quotechanger.web;

import com.kovunov.quotechanger.dto.TextDto;
import com.kovunov.quotechanger.entity.Quote;
import com.kovunov.quotechanger.entity.Response;
import com.kovunov.quotechanger.service.QuoteService;
import com.kovunov.quotechanger.service.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/quote")
public class QuoteController {

    @Autowired
    private QuoteService quoteService;

    @Autowired
    private ResponseService responseService;

    @PostMapping
    public Quote postQuote(@Valid @RequestBody TextDto textDto) {
        Quote quote = new Quote(textDto.getText());
        quoteService.saveOrUpdate(quote);
        return quote;
    }

    @PostMapping("/{quoteId}")
    public Response saveResponseForPost(@RequestBody TextDto textDto, @PathVariable String quoteId) {
        Quote quote = quoteService.findQuoteById(Integer.parseInt(quoteId));
        Response response = new Response(textDto.getText(), quote);
        responseService.saveOrUpdate(response);
        return response;
    }

    @GetMapping
    public List<Quote> getAllQuotes() {
        return quoteService.getAllQuotes();
    }
}
