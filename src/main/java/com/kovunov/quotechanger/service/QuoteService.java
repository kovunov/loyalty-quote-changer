package com.kovunov.quotechanger.service;

import com.kovunov.quotechanger.entity.Quote;
import com.kovunov.quotechanger.repository.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuoteService {

    @Autowired
    private QuoteRepository quoteRepository;

    public List<Quote> getAllQuotes() {
        List<Quote> quotes = new ArrayList<>();
        quoteRepository.findAll().forEach(quotes::add);
        return quotes;
    }

    public void saveOrUpdate(Quote quote) {
        quoteRepository.save(quote);
    }

    public Quote findQuoteById(int quoteId) {
        return quoteRepository.findById(quoteId).orElse(new Quote("random quote"));
    }
}
