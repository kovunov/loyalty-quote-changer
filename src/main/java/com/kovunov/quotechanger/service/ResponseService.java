package com.kovunov.quotechanger.service;

import com.kovunov.quotechanger.entity.Response;
import com.kovunov.quotechanger.repository.ResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResponseService {

    @Autowired
    private ResponseRepository responseRepository;

    public void saveOrUpdate(Response response) {
        responseRepository.save(response);
    }

    public List<Response> findResponseByQuote(int quoteId) {
        return responseRepository.findResponsesByQuote(quoteId);
    }


}
