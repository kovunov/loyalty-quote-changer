package com.kovunov.quotechanger.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TextDto {
    private String text;

    public TextDto(String text) {
        this.text = text;
    }
}
