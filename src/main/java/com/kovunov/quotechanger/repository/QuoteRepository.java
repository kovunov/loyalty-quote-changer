package com.kovunov.quotechanger.repository;

import com.kovunov.quotechanger.entity.Quote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuoteRepository extends CrudRepository<Quote, Integer> {
}
