package com.kovunov.quotechanger.repository;

import com.kovunov.quotechanger.entity.Response;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResponseRepository extends JpaRepository<Response, Integer> {
    @Query(value = "SELECT * FROM Response resp WHERE resp.quote_id = ?1",
    nativeQuery = true)
    List<Response> findResponsesByQuote(Integer quoteId);
}
